package converter.cbfeedback.mapper;

import converter.cbfeedback.domain.FeedBack;
import converter.cbfeedback.domain.entities.FeedBackEntity;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@DisplayName("Collecting state mapper")
class FeedBackMapperTest {

    private final EasyRandom rnd = new EasyRandom();
    private final FeedBackMapper testingMapper = new FeedBackMapperImpl();
    private final static DateTimeFormatter target = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");

    @Test
    @DisplayName("FeedBack mapping")
    void map() {
        FeedBackEntity source = rnd.nextObject(FeedBackEntity.class);

        FeedBack result = testingMapper.map(source);

        assertEquals(source.getId(), result.getId(), "Incorrect mapping of 'id' field");
        assertEquals(source.getToUser(), result.getToUser(), "Incorrect mapping of 'toUser' field");
        assertEquals(source.getRead(), result.getRead(), "Incorrect mapping of 'read' field");
        assertEquals(source.getComment(), result.getComment(), "Incorrect mapping of 'comment' field");
        assertEquals(dateResolver(source.getDate()), result.getDate(), "Incorrect mapping of 'date' field");
        assertEquals(source.getFromUser().getName(), result.getFromUser(), "Incorrect mapping of 'fromUser' field");
    }

    private String dateResolver(LocalDateTime date) {
        return date.format(target);
    }

}
