package converter.cbfeedback.mapper;

import converter.cbfeedback.domain.FeedBack;
import converter.cbfeedback.domain.entities.FeedBackEntity;
import converter.cbfeedback.domain.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper
public interface FeedBackMapper extends DirectMapper<FeedBackEntity, FeedBack>{

    @Override
    @Mappings(value = {
            @Mapping(target = FeedBack.Fields.fromUser, source = FeedBackEntity.Fields.fromUser +
                    "." + UserEntity.Fields.name),
    })
    FeedBack map(FeedBackEntity source);
}
