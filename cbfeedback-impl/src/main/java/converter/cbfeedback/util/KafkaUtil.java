package converter.cbfeedback.util;

import com.fasterxml.jackson.databind.util.ClassUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ErrorHandler;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Map;

import static java.lang.String.format;
import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.springframework.util.ObjectUtils.nullSafeToString;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class KafkaUtil {

    public static <T> ConsumerFactory<String, T> consumer(String bootstrapServers, Class<T> objectClass) {
        Assert.notNull(bootstrapServers, "BootstrapServers is not present");
        Assert.notNull(objectClass, "Object class is not present");
        Map<String, Object> config = Map.of(
                BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        JsonDeserializer<T> deserializer = new JsonDeserializer<>(objectClass, false);
        return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(), deserializer);
    }

    @SafeVarargs
    public static <T> ConcurrentKafkaListenerContainerFactory<String, T> consumerListener(
            ConsumerFactory<String, T> consumerFactory,
            Class<? extends Throwable>... trackErrors) {

        Assert.notNull(consumerFactory, "Consumer factory is not present");

        ErrorHandler handler = (e, record) -> log.error("Error while processing: " + nullSafeToString(record), e);
        if (trackErrors.length > 0) {
            handler = (e, record) -> {
                String message = nullSafeToString(record);
                if (ExceptionUtil.hasOneInCauses(e, Arrays.asList(trackErrors))) {
                    Throwable rootCause = ClassUtil.getRootCause(e);
                    String errorMsg = ClassUtil.exceptionMessage(rootCause);
                    log.warn(format("Error while processing: %s, record is %s", message, errorMsg));
                } else {
                    log.error("Error while processing: " + message, e);
                }
            };
        }

        var factory = new ConcurrentKafkaListenerContainerFactory<String, T>();
        factory.setConsumerFactory(consumerFactory);
        factory.setErrorHandler(handler);
        return factory;
    }

    public static <T> KafkaTemplate<String, T> producerTemplate(String bootstrapServers) {
        Map<String, Object> config = producerConfig(bootstrapServers);
        return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(config));
    }

    private static Map<String, Object> producerConfig(String bootstrapServers) {
        return Map.of(
                ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers,
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class,
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class
        );
    }
}
