package converter.cbfeedback.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExceptionUtil {

    public static boolean hasOneInCauses(Throwable t, List<Class<? extends Throwable>> goals) {
        if (t == null) {
            return false;
        }
        Throwable root = t;
        do {
            for (Class<? extends Throwable> goal : goals) {
                if (goal.isInstance(root)) {
                    return true;
                }
            }
        } while ((root = root.getCause()) != null);
        return false;
    }

}
