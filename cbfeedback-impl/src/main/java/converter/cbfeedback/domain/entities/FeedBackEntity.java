package converter.cbfeedback.domain.entities;

import converter.cbfeedback.domain.UserEntity;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Builder
@Table(name = "feedback")
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class FeedBackEntity {

    public FeedBackEntity(String comment, UserEntity fromUser, String toUser, LocalDateTime date, Boolean read) {
        this.comment = comment;
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.date = date;
        this.read = read;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "comment", nullable = false)
    private String comment;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "from_user", nullable = false, referencedColumnName = "id")
    @Fetch(FetchMode.JOIN)
    private UserEntity fromUser;

    @Column(name = "to_user", nullable = false)
    private String toUser;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "read", length = 5)
    private Boolean read;

}
