package converter.cbfeedback.service;

import converter.cbfeedback.domain.FeedBack;
import converter.cbfeedback.domain.entities.FeedBackEntity;
import converter.cbfeedback.domain.UserEntity;
import converter.cbfeedback.mapper.FeedBackMapper;
import converter.cbfeedback.repository.FeedBackRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

import static java.lang.String.format;

@Slf4j
@Service
@RequiredArgsConstructor
public class FeedBackServiceImpl implements FeedBackService {

    private final FeedBackRepository feedBackRepository;
    private final FeedBackMapper mapper;

    @Override
    @Transactional
    public void addComment(UserEntity user, String comment) {
            FeedBackEntity feedBack = new FeedBackEntity(
                    comment,
                    user,
                    "admin",
                    LocalDateTime.now(),
                    false);
            log.info("Добавление нового отзыва: от '{}' к '{}' время: '{}'",
                    feedBack.getFromUser(), feedBack.getToUser(), feedBack.getDate());
            feedBackRepository.save(feedBack);
    }

//    @Override
//    public List<FeedBack> getOutboxComments() {
//        List<FeedBackEntity> comments = feedBackRepository.getAllByFromUserOrderByDate(userService.getCurrentUser());
//        log.info("find comments: '{}'", comments.size());
//        return mapper.map(comments);
//    }

    @Override
    public List<FeedBack> getAllComments() {
        List<FeedBackEntity> comments = feedBackRepository.getAllByOrderByDate();
        log.info("find comments: '{}'", comments.size());
        return mapper.map(comments);
    }

    @Override
    public FeedBackEntity getById(Integer id) {
        return feedBackRepository.getById(id);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        feedBackRepository.deleteById(id);
        log.info("Отзыв успешно удален");
    }
}
