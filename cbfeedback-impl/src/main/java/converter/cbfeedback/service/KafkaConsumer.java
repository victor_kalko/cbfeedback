package converter.cbfeedback.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KafkaConsumer {

    @KafkaListener(topics="topic_1", groupId = "group_id")
    public void consumer(String message){
        log.info("message: '{}'", message);
    }

}
