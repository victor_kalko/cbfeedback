package converter.cbfeedback.service;


import converter.cbfeedback.domain.FeedBack;
import converter.cbfeedback.domain.entities.FeedBackEntity;
import converter.cbfeedback.domain.UserEntity;

import java.util.List;

public interface FeedBackService {

    /**
     * <p>Добавить отзыв.</p>
     * @param comment  комментарий, который привязывается к Admin (пока что)
     **/
    void addComment(UserEntity user, String comment);

    /**
     * <p>Получить все свои комментарии отсортированные по дате.</p>
     * @return список отзывов.
     **/
//    List<FeedBack> getOutboxComments();

    /**
     * <p>Получить все комментарии отсортированные по дате.</p>
     * @return список отзывов.
     **/
    List<FeedBack> getAllComments();

    /**
     * <p>Получить отзыв по уникальному идентификатору.</p>
     * @return отзыв.
     **/
    FeedBackEntity getById(Integer id);

    /**
     * <p>Удалить отзыв по уникальному идентификатору.</p>
     **/
    void deleteById(Integer id);
}
