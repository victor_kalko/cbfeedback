package converter.cbfeedback.config;

import converter.cbfeedback.mapper.FeedBackMapper;
import converter.cbfeedback.mapper.FeedBackMapperImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public FeedBackMapper feedBackMapper() {
        return new FeedBackMapperImpl();
    }

}
