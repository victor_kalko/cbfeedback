package converter.cbfeedback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@SpringBootApplication
//@EnableEurekaClient
public class CbfeedbackApplication {
                                                                                                                                        
    public static void main(String[] args) {
        SpringApplication.run(CbfeedbackApplication.class, args);
    }

}
