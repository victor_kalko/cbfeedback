package converter.cbfeedback;

import converter.cbfeedback.domain.FeedBack;
import converter.cbfeedback.domain.UserEntity;
import converter.cbfeedback.service.FeedBackService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import converter.cbfeedback.feign.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class FeedbackFeignImpl implements FeedbackFeign {
    private final FeedBackService feedBackService;

    @Override
    public List<FeedBack> getComments() {
        return feedBackService.getAllComments();
    }

    @Override
    public void addComment(@NonNull UserEntity user,
                             String comment) {
        feedBackService.addComment(user, comment);
    }

    @Override
    public void deleteById(Integer id) {
        feedBackService.deleteById(id);
    }
}
