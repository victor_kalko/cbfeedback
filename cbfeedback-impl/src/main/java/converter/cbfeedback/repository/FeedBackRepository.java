package converter.cbfeedback.repository;

import converter.cbfeedback.domain.entities.FeedBackEntity;
import converter.cbfeedback.domain.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeedBackRepository extends CrudRepository<FeedBackEntity, Long> {

    List<FeedBackEntity> getAllByFromUserOrderByDate(UserEntity user);

    List<FeedBackEntity> getAllByOrderByDate();

    void deleteById(Integer id);

    FeedBackEntity getById(Integer id);
}
