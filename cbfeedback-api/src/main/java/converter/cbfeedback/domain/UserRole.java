package converter.cbfeedback.domain;

import lombok.experimental.FieldNameConstants;
import org.springframework.security.core.GrantedAuthority;

@FieldNameConstants
public enum UserRole implements GrantedAuthority {
    ADMIN,
    USER;

    @Override
    public String getAuthority() {
        return name();
    }
}
