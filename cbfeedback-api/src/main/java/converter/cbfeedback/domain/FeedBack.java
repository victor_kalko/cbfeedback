package converter.cbfeedback.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class FeedBack {

    private Integer id;

    private String comment;

    private String fromUser;

    private String toUser;

    private String date;

    private Boolean read;

}
