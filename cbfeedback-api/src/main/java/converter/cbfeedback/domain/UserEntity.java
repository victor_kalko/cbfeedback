package converter.cbfeedback.domain;

import lombok.*;
import lombok.experimental.FieldNameConstants;

import javax.persistence.*;

import static java.lang.String.format;

@Entity
@Getter
@Setter
@Builder
@Table(name = "users")
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {

    public UserEntity(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "active", nullable = false)
    private boolean active;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "activation_code")
    private String activationCode;

    @Column(name = "role", length = 5)
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Override
    public String toString() {
        return format("id = '%s', name = '%s', active = '%s'", this.id, this.name, this.active);
    }
}
