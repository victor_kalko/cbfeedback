package converter.cbfeedback.feign;

import converter.cbfeedback.domain.FeedBack;
import converter.cbfeedback.domain.UserEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static converter.cbfeedback.feign.FeedbackApiDefinition.FEEDBACK_NAME;
import static converter.cbfeedback.feign.FeedbackApiDefinition.FEEDBACK_ROUTING;


@FeignClient(name = FEEDBACK_NAME, url = FEEDBACK_ROUTING)
public interface FeedbackFeign{

    @GetMapping(value = "/feedback/getComments")
    List<FeedBack> getComments();

    @PostMapping(value = "/feedback/addComment")
    void addComment(
            @RequestBody UserEntity user,
            @RequestParam String comment);

    @PostMapping(value = "/feedback/deleteById")
    void deleteById(
            @RequestParam Integer id);
}
